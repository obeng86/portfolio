import { useState } from 'react'
import contactStyles from '../styles/contact.module.css'

export default function Contact() {
    // Form field states
    const [name, setName] = useState('')
    const [email, setEmail] = useState('')
    const [message, setMessage] = useState('')

    //Form Validation state
    const [errors, setErrors] = useState({});

    //   Setting button text on form submission
    const [buttonText, setButtonText] = useState(`<em class="em1">S</em><em class="em2">E</em><em class="em3">N</em><em class="em4">D</em><em class="em5"></em><em class="em6">M</em><em class="em7">E</em><em class="em8">S</em><em class="em9">S</em><em class="em10">A</em><em class="em10">G</em><em class="em10">E</em>`);

    // Setting success or failure messages states
    const [showSuccessMessage, setShowSuccessMessage] = useState(false);
    const [showFailureMessage, setShowFailureMessage] = useState(false);

    //Validation check method
    const handleValidation = () => {
        let tempErrors = {};
        let isValid = true;

        if (name.length <= 0) {
            tempErrors["name"] = true;
            isValid = false;
        }
        if (email.length <= 0) {
            tempErrors["email"] = true;
            isValid = false;
        }
        if (message.length <= 0) {
            tempErrors["message"] = true;
            isValid = false;
        }

        setErrors({ ...tempErrors });
        return isValid;
    };

    //Form Submit Handler
    const handleSubmit = async (e) => {
        e.preventDefault();

        let isValidForm = handleValidation();

        if (isValidForm) {
            setButtonText(`<em class="em1">S</em><em class="em2">E</em><em class="em3">N</em><em class="em4">D</em><em class="em5">I</em><em class="em6">N</em><em class="em7">G</em>`);
            const data = {
                name,
                email,
                message,
              };
            const res = await fetch('/api/sendgrid', {
                method: 'post',
                body: JSON.stringify(data),
                headers: {
                    "Content-Type": "application/json",
                }
              });

              const { error } = await res.json();

              if (error) {
                console.log(error);
                setShowSuccessMessage(false);
                setShowFailureMessage(true);
                setButtonText(`<em class="em1">S</em><em class="em2">E</em><em class="em3">N</em><em class="em4">D</em><em class="em5"></em><em class="em6">M</em><em class="em7">E</em><em class="em8">S</em><em class="em9">S</em><em class="em10">A</em><em class="em11">G</em><em class="em12">E</em>`)
                return;
            }
            setShowSuccessMessage(true);
            setShowFailureMessage(false);
            setButtonText(`<em class="em1">S</em><em class="em2">E</em><em class="em3">N</em><em class="em4">D</em><em class="em5"></em><em class="em6">M</em><em class="em7">E</em><em class="em8">S</em><em class="em9">S</em><em class="em10">A</em><em class="em11">G</em><em class="em12">E</em>`);
            // Reset form fields
            setName('');
            setEmail('');
            setMessage('');
        }
          console.log(name, email, message);
      };

    return (
        <section id="contactSection" className={contactStyles.contactWrapper}>
            <div className={contactStyles.container}>
                <div className={contactStyles.content}>
                    <div className={contactStyles.title}>
                        <h2 className={contactStyles.h2Title}><span>Contact</span></h2>
                        <div className={contactStyles.subtitle}><span>LET'S TALK</span></div>
                    </div>
                    <div className={contactStyles.form}>
                        <form onSubmit={handleSubmit}>
                            <div className={contactStyles.formGroup}>
                                <div className={contactStyles.label}>Full Name <strong className={contactStyles.strong}>*</strong></div>
                                <p className={contactStyles.para}>
                                    <span className={contactStyles.span}>
                                        <input onChange={(e)=>{setName(e.target.value)}} className={contactStyles.inputText} name="name" type="text" placeholder="Hannibal Barca" value={name}/>
                                    </span>
                                </p>
                                {errors?.name && (
                                <p className={contactStyles.warning}>Please enter your name</p>
                                )}
                            </div>
                            <div className={contactStyles.formGroup}>
                                <div className={contactStyles.label}>Email Address <strong className={contactStyles.strong}>*</strong></div>
                                <p className={contactStyles.para}>
                                    <span className={contactStyles.span}>
                                        <input onChange={(e)=>{setEmail(e.target.value)}} className={contactStyles.inputText} name="email" type="email" placeholder="hbarca@domain.com" value={email}/>
                                    </span>
                                </p>
                                {errors?.email && (
                                <p className={contactStyles.warning}>Please enter an email</p>
                                )}
                            </div>
                            <div className={contactStyles.formGroup}>
                                <div className={contactStyles.label}>Message <strong className={contactStyles.strong}>*</strong></div>
                                <p className={contactStyles.para}>
                                    <span className={contactStyles.span}>
                                        <textarea onChange={(e)=>{setMessage(e.target.value)}} className={contactStyles.textArea} name="message" cols="40" rows="10" placeholder="How can I help?" value={message}></textarea>
                                    </span>
                                </p>
                                {errors?.message && (
                                <p className={contactStyles.warning}>Please enter a message</p>
                                )}
                            </div>
                            <button id="formBtn" className={contactStyles.formBtn} type="submit">
                                <div id="submitBtn" className={contactStyles.submitBtn}>
                                <span className={contactStyles.btnAnimation}>
                                    <span className={contactStyles.submitBtnSpan} dangerouslySetInnerHTML={{ __html: buttonText }} />
                                </span>
                                <i id="icon" className={contactStyles.icon + ` fas fa-chevron-right`}></i>
                                </div>
                            </button>
                            {showSuccessMessage && (
                            <p className={contactStyles.success}>
                                Message delivered
                            </p>
                            )}
                            {showFailureMessage && (
                            <p className={contactStyles.warning}>
                                Oops! Something went wrong, please try again.
                            </p>
                            )}
                        </form>
                    </div>
                    <div className={contactStyles.info}>
                        <div className={contactStyles.name}>Michael Squire Obeng</div>
                        <div className={contactStyles.subName}>Web Developer & Mentor</div>
                        <div className={contactStyles.infoList}>
                            <ul className={contactStyles.unorderedList}>
                                <li className={contactStyles.listItem}>
                                    <strong className={contactStyles.strongListItem}><span>Residence:</span></strong><span>UK</span>
                                </li>
                                <li className={contactStyles.listItem}>
                                    <strong className={contactStyles.strongListItem}><span>Freelance:</span></strong><span>Available</span>
                                </li>
                                <li className={contactStyles.listItem}>
                                    <strong className={contactStyles.strongListItem}><span>Address:</span></strong><span>London</span>
                                </li>
                            </ul>
                        </div>
                        <div className={contactStyles.author}>M. S. Obeng</div>
                        <div className={contactStyles.clear}></div>
                    </div>
                    <div className={contactStyles.clear}></div>
                </div>
            </div>
        </section>
    )
}