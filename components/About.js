import aboutSectionStyles from '../styles/about.module.css'

export default function About() {
    return (
        <section id="aboutSection" className={aboutSectionStyles.aboutWrapper}>
            <div className={aboutSectionStyles.container}>
                <div className={aboutSectionStyles.content}>
                    <div className={aboutSectionStyles.title}>
                        <h2 className={aboutSectionStyles.h2Title}><span>About Me</span></h2>
                        <div className={aboutSectionStyles.subtitle}><span>MY STORY</span></div>
                    </div>
                    <div className={aboutSectionStyles.personalStatement}>
                        <p className={aboutSectionStyles.paragraph}>I started as a Web Content Editor at London Business School, shortly thereafter taking a Front End role with Signature Gifts - an ecommerce firm in St Albans.  I'm currently in Accra, Ghana </p>
                        {/* <p className={aboutSectionStyles.paragraph}>My skillset has broadened to include social media marketing and graphic design.  <b>What does that mean for you?</b> Working with me not only gets you an experienced web developer but one that understands how social media and tasteful design delivers more sales, results and growth.</p> */}
                    </div>
                    <div className={aboutSectionStyles.personalInfo}>
                        <ul className={aboutSectionStyles.unorderedList}>
                            <li className={aboutSectionStyles.listItem}>
                                <strong className={aboutSectionStyles.strongTag}>
                                    <span>Residence:</span>
                                </strong>
                                <span>UK</span>
                            </li>
                            <li className={aboutSectionStyles.listItem}>
                                <strong className={aboutSectionStyles.strongTag}>
                                    <span>Freelance:</span>
                                </strong>
                                <span>Available</span>
                            </li>
                            <li className={aboutSectionStyles.listItem}>
                                <strong className={aboutSectionStyles.strongTag}>
                                    <span>Address:</span>
                                </strong>
                                <span>London</span>
                            </li>
                            <li className={aboutSectionStyles.listItem}>
                                <strong className={aboutSectionStyles.strongTag}>
                                    <span>Salesforce:</span>
                                </strong>
                                <span><a href="https://trailblazer.me/id/mobeng2" target="_blank" >/mobeng2</a></span>
                            </li>
                            <li className={aboutSectionStyles.listItem}>
                                <strong className={aboutSectionStyles.strongTag}>
                                    <span>Github:</span>
                                </strong>
                                <span><a href="https://github.com/mikeydoesit/" target="_blank" >/mikeydoesit</a></span>
                            </li>
                            <li className={aboutSectionStyles.listItem}>
                                <strong className={aboutSectionStyles.strongTag}>
                                    <span>LinkedIn:</span>
                                </strong>
                                <span><a href="https://www.linkedin.com/in/michaelobeng/" target="_blank">/michaelobeng</a></span>
                            </li>
                        </ul>
                    </div>
                    <div className={aboutSectionStyles.clear}></div>
                </div>
            </div>
        </section>
    )
}