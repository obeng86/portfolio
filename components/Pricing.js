import React, { useCallback } from "react"
import { useEmblaCarousel } from 'embla-carousel/react'
import pricingSectionStyles from '../styles/pricing.module.css'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome"
import { faCar, faChevronLeft, faChevronRight, faPlane, faRocket } from "@fortawesome/free-solid-svg-icons";

export default function Pricing () {
    
    const [emblaRef, emblaApi] = useEmblaCarousel()

    const scrollPrev = useCallback(() => {    if (emblaApi) emblaApi.scrollPrev()  }, [emblaApi])
    const scrollNext = useCallback(() => {    if (emblaApi) emblaApi.scrollNext()  }, [emblaApi])

    return (
        <section className={pricingSectionStyles.wrapper}>
            <div className={pricingSectionStyles.container}>
                <div className={pricingSectionStyles.content}>
                    <div className={pricingSectionStyles.title}>
                        <h2 className={pricingSectionStyles.h2Title}><span>Pricing</span></h2>
                        <div className={pricingSectionStyles.subtitle}><span>MY PLANS</span></div>
                    </div>
                    <div className={pricingSectionStyles.contentCarousel}>
                    <div className="embla">
                        <div className="embla_viewport" ref={emblaRef}>
                            <div className="embla__container">
                            <div className="embla__slide">
                                <div className={pricingSectionStyles.pricingItem}>
                                    <div className={pricingSectionStyles.icon}>
                                        <FontAwesomeIcon icon={faCar} className={pricingSectionStyles.pricingSvg} />
                                    </div>
                                    <div className={pricingSectionStyles.name}>
                                        <span>Starter</span>
                                    </div>
                                    <div className={pricingSectionStyles.amount}>
                                        <span className={pricingSectionStyles.priceWrapper}>
                                            <span className={pricingSectionStyles.currency}>£</span>
                                            <span className={pricingSectionStyles.integer}>750</span>
                                        </span>
                                    </div>
                                    <div className={pricingSectionStyles.features}>
                                        <ul className={pricingSectionStyles.list}>
                                            <li className={pricingSectionStyles.listItem}>15GB Storage</li>
                                            <li className={pricingSectionStyles.listItem}>6 Pages</li>
                                            <li className={pricingSectionStyles.listItem}>Standard Designs</li>
                                            <li className={pricingSectionStyles.listItem}>No Ecommerce</li>
                                            <li className={pricingSectionStyles.listItem}>5 Email Accounts</li>
                                            <li className={pricingSectionStyles.listItem}>£5/mo Managed Hosting</li>
                                            <li className={pricingSectionStyles.listItem}>Social Media Audience Research</li>
                                            <li className={pricingSectionStyles.listItem}>Google Analytics Monthly Reports</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div className="embla__slide">
                                <div className={pricingSectionStyles.pricingItem}>
                                    <div className={pricingSectionStyles.icon}>
                                        <FontAwesomeIcon icon={faPlane} className={pricingSectionStyles.pricingSvg} />
                                    </div>
                                    <div className={pricingSectionStyles.name}>
                                        <span>Premium</span>
                                    </div>
                                    <div className={pricingSectionStyles.amount}>
                                        <span className={pricingSectionStyles.priceWrapper}>
                                            <span className={pricingSectionStyles.currency}>£</span>
                                            <span className={pricingSectionStyles.integer}>1350</span>
                                        </span>
                                    </div>
                                    <div className={pricingSectionStyles.features}>
                                        <ul className={pricingSectionStyles.list}>
                                            <li className={pricingSectionStyles.listItem}>25GB Storage</li>
                                            <li className={pricingSectionStyles.listItem}>10 Pages</li>
                                            <li className={pricingSectionStyles.listItem}>Premium Designs</li>
                                            <li className={pricingSectionStyles.listItem}>Ecommerce</li>
                                            <li className={pricingSectionStyles.listItem}>10 Email Accounts</li>
                                            <li className={pricingSectionStyles.listItem}>£15/mo Managed Hosting</li>
                                            <li className={pricingSectionStyles.listItem}>Social Media Audience Research</li>
                                            <li className={pricingSectionStyles.listItem}>Social Media Campaign Setup</li>
                                            <li className={pricingSectionStyles.listItem}>Google Analytics Monthly Reports</li>
                                            <li className={pricingSectionStyles.listItem}>Logo Design</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                            <div className="embla__slide">
                                <div className={pricingSectionStyles.pricingItem}>
                                    <div className={pricingSectionStyles.icon}>
                                        <FontAwesomeIcon icon={faRocket} className={pricingSectionStyles.pricingSvg} />
                                    </div>
                                    <div className={pricingSectionStyles.name}>
                                        <span>Elite</span>
                                    </div>
                                    <div className={pricingSectionStyles.amount}>
                                        <span className={pricingSectionStyles.priceWrapper}>
                                            <span className={pricingSectionStyles.currency}>£</span>
                                            <span className={pricingSectionStyles.integer}>2500</span>
                                        </span>
                                    </div>
                                    <div className={pricingSectionStyles.features}>
                                        <ul className={pricingSectionStyles.list}>
                                            <li className={pricingSectionStyles.listItem}>50GB Storage</li>
                                            <li className={pricingSectionStyles.listItem}>15 Pages</li>
                                            <li className={pricingSectionStyles.listItem}>Premium Designs</li>
                                            <li className={pricingSectionStyles.listItem}>Ecommerce</li>
                                            <li className={pricingSectionStyles.listItem}>20 Email Accounts</li>
                                            <li className={pricingSectionStyles.listItem}>£30/mo Managed Hosting</li>
                                            <li className={pricingSectionStyles.listItem}>Social Media Audience Research</li>
                                            <li className={pricingSectionStyles.listItem}>Social Media Campaign Setup</li>
                                            <li className={pricingSectionStyles.listItem}>Google Analytics Monthly Reports</li>
                                            <li className={pricingSectionStyles.listItem}>Logo Design</li>
                                            <li className={pricingSectionStyles.listItem}>Professional Photography</li>
                                            <li className={pricingSectionStyles.listItem}>Custom Designed Adverts</li>
                                            <li className={pricingSectionStyles.listItem}>Digital Strategy</li>
                                            <li className={pricingSectionStyles.listItem}>Weekly Meetings</li>
                                        </ul>
                                    </div>
                                </div>
                            </div>
                        </div>
                    </div>
                        <div className={pricingSectionStyles.carouselNav}>
                            <span className={pricingSectionStyles.span + ` embla__prev`} onClick={scrollPrev}>
                                <FontAwesomeIcon icon={faChevronLeft} size="xs" className={pricingSectionStyles.svg} />
                            </span>
                            <span className={pricingSectionStyles.span + ` embla__next`} onClick={scrollNext}>
                                <FontAwesomeIcon icon={faChevronRight} size="xs" className={pricingSectionStyles.svg} />
                            </span>
                        </div>
                    </div>
                        
                    </div>
                </div>
            </div>
        </section>
    )
}
