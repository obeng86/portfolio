import portfolioStyles from '../styles/portfolio.module.css'
import Projects from '../components/Projects'
import Image from 'next/image'
import { Component } from 'react';


class Portfolio extends Component {

    constructor() {
        super();

        this.state = {
            displayedItems: []
        }
        this.allFilter = this.allFilter.bind(this)
        this.wordpressFilter = this.wordpressFilter.bind(this)
        this.reactFilter = this.reactFilter.bind(this)
        this.shopifyFilter = this.shopifyFilter.bind(this)
        this.artworkFilter = this.artworkFilter.bind(this)
    }


    componentDidMount() {
        this.allFilter()
    }
    
    allFilter() {
        const menuLinks = document.querySelector('[class^="portfolio_filterMenu_"]').children
        for (let i=0; i < menuLinks.length; i++) {
            if (menuLinks[i].id == 'all') {
                menuLinks[i].style.color = '#68e0cf'
            } else {
                menuLinks[i].style.color = '#ffffff'
            }
        }
        console.log(menuLinks)
        const items = Projects
        this.setState({displayedItems: items})
    }
    wordpressFilter() {
        const menuLinks = document.querySelector('[class^="portfolio_filterMenu_"]').children
        for (let i=0; i < menuLinks.length; i++) {
            if (menuLinks[i].id == 'wordpress') {
                menuLinks[i].style.color = '#68e0cf'
            } else {
                menuLinks[i].style.color = '#ffffff'
            }
        }
        const items = Projects.filter(item => {
            return item.category == 'wordpress'
        })
        this.setState({displayedItems: items})
    }
    reactFilter() {
        const menuLinks = document.querySelector('[class^="portfolio_filterMenu_"]').children
        for (let i=0; i < menuLinks.length; i++) {
            if (menuLinks[i].id == 'react') {
                menuLinks[i].style.color = '#68e0cf'
            } else {
                menuLinks[i].style.color = '#ffffff'
            }
        }
        const items = Projects.filter(item => {
            return item.category == 'react'
        })
        this.setState({displayedItems: items})
    }
    shopifyFilter() {
        const menuLinks = document.querySelector('[class^="portfolio_filterMenu_"]').children
        for (let i=0; i < menuLinks.length; i++) {
            if (menuLinks[i].id == 'shopify') {
                menuLinks[i].style.color = '#68e0cf'
            } else {
                menuLinks[i].style.color = '#ffffff'
            }
        }
        const items = Projects.filter(item => {
            return item.category == 'shopify'
        })
        this.setState({displayedItems: items})
    }
    artworkFilter() {
        const menuLinks = document.querySelector('[class^="portfolio_filterMenu_"]').children
        for (let i=0; i < menuLinks.length; i++) {
            if (menuLinks[i].id == 'artwork') {
                menuLinks[i].style.color = '#68e0cf'
            } else {
                menuLinks[i].style.color = '#ffffff'
            }
        }
        const items = Projects.filter(item => {
            return item.category == 'artwork'
        })
        this.setState({displayedItems: items})
    }


    render() {
        return (
            <section id="portfolioSection" className={portfolioStyles.portfolioWrapper}>
                <div className={portfolioStyles.container}>
                    <div className={portfolioStyles.content}>
                        <div className={portfolioStyles.title}>
                            <h2 className={portfolioStyles.h2Title}><span>Portfolio</span></h2>
                            <div className={portfolioStyles.subtitle}><span>LATEST WORKS</span></div>
                        </div>
                        <div className={portfolioStyles.filterMenu}>
                            <div id="all" className={portfolioStyles.menuItem} onClick={this.allFilter}>
                                <span className={portfolioStyles.span}>
                                    All
                                </span>
                            </div>
                            <div id="wordpress" className={portfolioStyles.menuItem} onClick={this.wordpressFilter}>
                                <span className={portfolioStyles.span}>
                                    Wordpress
                                </span>
                            </div>
                            <div id="react" className={portfolioStyles.menuItem} onClick={this.reactFilter}>
                                <span className={portfolioStyles.span}>
                                    React JS
                                </span>
                            </div>
                            <div id="shopify" className={portfolioStyles.menuItem} onClick={this.shopifyFilter}>
                                <span className={portfolioStyles.span}>
                                    Shopify
                                </span>
                            </div>
                            <div id="artwork" className={portfolioStyles.menuItem} onClick={this.artworkFilter}>
                                <span className={portfolioStyles.span}>
                                    Artwork
                                </span>
                            </div>
                        </div>
                        <div className={portfolioStyles.grid}>
                            {this.state.displayedItems.map(project => (
                                
                                <div data-category={project.category} className={portfolioStyles.gridItem}>
                                    <Image src={project.img} layout="fill" objectFit="cover" objectPosition="center center" priority='true'/>
                                    <a href={project.url ? project.url : null} target="_blank" className={portfolioStyles.overlay}>
                                    <i className={project.fontawesome} />
                                    </a>
                                </div>
                            ))}
                        </div>
                    </div>
                </div>
            </section>
        )
    }   
}
export default Portfolio