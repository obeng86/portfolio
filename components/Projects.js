const Projects = [
    {
        type: 'link',
        url: 'https://www.kafuirestaurant.com',
        img: '/assets/kafuirestaurant.png',
        fontawesome: 'fab fa-wordpress-simple fa-lg',
        category: 'wordpress'
    },
    {
        type: 'link',
        url: 'https://www.duabaexpress.com',
        img: '/assets/duabaexpress.png',
        fontawesome: 'fab fa-wordpress-simple fa-lg',
        category: 'wordpress'
    },
    {
        type: 'logo',
        url: '/assets/flamboyant.png',
        img: '/assets/flamboyant.png',
        fontawesome: 'fas fa-paint-brush fa-lg',
        category: 'artwork'
    },
    {
        type: 'flyer',
        url: '/assets/motherofdivine.png',
        img: '/assets/motherofdivine.png',
        fontawesome: 'fas fa-paint-brush fa-lg',
        category: 'artwork'
    },
    {
        type: 'flyer',
        url: '/assets/aaimish.png',
        img: '/assets/aaimish.png',
        fontawesome: 'fas fa-paint-brush fa-lg',
        category: 'artwork'
    },
    {
        type: 'link',
        url: 'https://salarycruncher.vercel.app/',
        img: '/assets/salaryCrunch.png',
        fontawesome: 'fab fa-react fa-lg',
        category: 'react'
    }
]

export default Projects