import Image from 'next/image'
import headerStyles from '../styles/header.module.css'
import monogram from '../public/MSOmonogram.png'
import { FontAwesomeIcon } from '@fortawesome/react-fontawesome'

export default function Header () {

    const toggleMobileMenu = () => {
        const mobileMenu = document.querySelector('[class^="header_mainMenu_"]')
        const menuWrapper = document.querySelector('[class^="header_mainWrapper_"]')
        
        if (mobileMenu.id == 'active' ) {
            mobileMenu.removeAttribute('id')
            menuWrapper.removeAttribute('id')
          } else {
            mobileMenu.id = 'active'
            menuWrapper.id = 'activate'
          }
    }

    const collapseMenuOnHeaderLinkClick = () => {
        const mobileMenu = document.querySelector('[class^="header_mainMenu_"]')
        const menuWrapper = document.querySelector('[class^="header_mainWrapper_"]')
        
        if (document.body.clientWidth <= 991.98) {
            mobileMenu.removeAttribute('id')
            menuWrapper.removeAttribute('id')
        }
    }
    return (
        <header className={headerStyles.mainWrapper}>
            <div className={headerStyles.logo}>
                <a href="/">
                    <Image  className={headerStyles.logoImg}
                            src={monogram}
                            alt="Michael Squire Obeng Monogram"
                            width={36}
                            height={36}
                    />
                    <span className={headerStyles.logoText}>Michael<br/>Squire Obeng</span>
                </a>
            </div>
            <a onClick={toggleMobileMenu} id="mobileMenuBtn" className={headerStyles.mobileMenuBtn} href="#">
                <span className={headerStyles.mobileMenuSpan}></span>
            </a>
            
            <div className={headerStyles.mainMenu}>
                <ul className={headerStyles.menuList}>
                    <li className={headerStyles.menuListItem} id="home">
                        <a onClick={collapseMenuOnHeaderLinkClick} href="/">
                            <span className={headerStyles.btnAnimation}>
                                <span className={headerStyles.menuBtnSpan}>
                                    <em className="em1">H</em>
                                    <em className="em2">O</em>
                                    <em className="em3">M</em>
                                    <em className="em4">E</em>
                                </span>
                            </span>
                        </a>
                    </li>
                    <li className={headerStyles.menuListItem} id="about">
                        <a onClick={collapseMenuOnHeaderLinkClick} href="#aboutSection">
                            <span className={headerStyles.btnAnimation}>
                                <span className={headerStyles.menuBtnSpan}>
                                    <em className="em1">A</em>
                                    <em className="em2">B</em>
                                    <em className="em3">O</em>
                                    <em className="em4">U</em>
                                    <em className="em5">T</em>
                                </span>
                            </span>
                        </a>
                    </li>
                    <li className={headerStyles.menuListItem} id="portfolio">
                        <a onClick={collapseMenuOnHeaderLinkClick} href="#portfolioSection">
                            <span className={headerStyles.btnAnimation}>
                                <span className={headerStyles.menuBtnSpan}>
                                    <em className="em1">P</em>
                                    <em className="em2">O</em>
                                    <em className="em3">R</em>
                                    <em className="em4">T</em>
                                    <em className="em5">F</em>
                                    <em className="em6">O</em>
                                    <em className="em7">L</em>
                                    <em className="em8">I</em>
                                    <em className="em9">O</em>
                                </span>
                            </span>
                        </a>
                    </li>
                    <li className={headerStyles.menuListItem} id="contact">
                        <a onClick={collapseMenuOnHeaderLinkClick} href="#contactSection">
                            <span className={headerStyles.btnAnimation}>
                                <span className={headerStyles.menuBtnSpan}>
                                    <em className="em1">C</em>
                                    <em className="em2">O</em>
                                    <em className="em3">N</em>
                                    <em className="em4">T</em>
                                    <em className="em5">A</em>
                                    <em className="em6">C</em>
                                    <em className="em7">T</em>
                                </span>
                            </span>
                        </a>
                    </li>
                </ul>
            </div>
        </header>
    )
}