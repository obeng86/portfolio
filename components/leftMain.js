import leftMainStyles from '../styles/leftMain.module.css'

export default function leftMain() {
    return (
        <div className={leftMainStyles.mainWrapper}>
            <div className={leftMainStyles.main}>
                <div className={leftMainStyles.img}></div>
            </div>
        </div>
        
    )
}