import React from "react";
import 'animate.css/animate.min.css'
import welcomeSectionStyles from '../styles/welcome.module.css'
import Typed from 'typed.js';
import Roles from '../components/roles'
import { FontAwesomeIcon } from "@fortawesome/react-fontawesome";
import { faChevronDown } from "@fortawesome/free-solid-svg-icons";


class Welcome extends React.Component {
   
  componentDidMount() {

    const options = {
      strings: Roles,
      typeSpeed: 50,
      backSpeed: 50,
      loop: true,
      cursorChar: "|",
    };
    // this.el refers to the <span> in the render() method
    this.typed = new Typed(this.el, options);

  }

  componentWillUnmount() {
    // Please don't forget to cleanup animation layer
    this.typed.destroy();
  }


  
  render() {
    return (
      <section  id="welcome" className={welcomeSectionStyles.wrapper}>
        <div className={welcomeSectionStyles.container}>
            <div className={welcomeSectionStyles.welcomeSection}>
                <div className={welcomeSectionStyles.centrize}>
                    <div className={welcomeSectionStyles.verticalCentrize}>
                        <h1 className={welcomeSectionStyles.h1Title}>
                            <span>Michael<br />Squire Obeng</span>
                        </h1>
                        <div className={welcomeSectionStyles.content}>
                            <div className={welcomeSectionStyles.subtitle}>
                                <span className={welcomeSectionStyles.typedSubtitle}>
                                    <span
                                    style={{ whiteSpace: "pre" }}
                                    ref={(el) => {
                                        this.el = el;
                                    }}
                                    />
                                </span>
                            </div>
                            <div className={welcomeSectionStyles.text}>
                                <p className={welcomeSectionStyles.paragraph}>
                                    Hi there! I am a Web Developer from London.  I have 5+ years in website design and development.  I am good at customising and developing Wordpress themes. I'm also comfortable building applications with Javascript frameworks.
                                </p>
                            </div>
                            <a id="contactBtn" className={welcomeSectionStyles.contactBtn}>
                                <span className={welcomeSectionStyles.btnAnimation}>
                                    <span className={welcomeSectionStyles.welcomeBtnSpan}>
                                        <em className="em1">C</em>
                                        <em className="em2">O</em>
                                        <em className="em3">N</em>
                                        <em className="em4">T</em>
                                        <em className="em5">A</em>
                                        <em className="em6">C</em>
                                        <em className="em7">T</em>
                                        <em className="em8"></em>
                                        <em className="em9">M</em>
                                        <em className="em10">E</em>
                                    </span>
                                </span>
                            </a>
                            {/* <ScrollAnimation animateIn='fadeIn' animateOut='fadeOut'>
                            <a className={welcomeSectionStyles.chevronDown}>
                                <FontAwesomeIcon icon={faChevronDown} className={welcomeSectionStyles.svg} />
                            </a>
                            </ScrollAnimation> */}
                        </div>
                    </div>
                </div>
            </div>
        </div>
    </section>
    );
  }
}

export default Welcome;