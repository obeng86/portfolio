const mail = require('@sendgrid/mail');

mail.setApiKey(process.env.SENDGRID_API_KEY);

export default async (req, res) => {
  const body = req.body;

  const message = `
    Name: ${body.name}\r\n
    Email: ${body.email}\r\n
    Message: ${body.message}
  `;

  const data = {
    to: 'michaelsquireobeng@gmail.com',
    from: 'ghanareallyisashithole@gmail.com',
    subject: `New message from ${body.name}`,
    text: message,
    html: message.replace(/\r\n/g, '<br />'),
  };

  try {
    await mail.send(data);
  } catch (error) {
      return res.status(error.statusCode || 500).json({error: error.message});
  }

  res.status(200).json({ error: '' });
};