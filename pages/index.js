import Head from 'next/head'
import LeftMain from '../components/leftMain'
import Header from '../components/Header'
import Welcome from '../components/Welcome'
import About from '../components/About'
import Portfolio from '../components/Portfolio'
import Contact from '../components/Contact'
// import Pricing from '../components/Pricing'
import styles from '../styles/Home.module.css'

export default function Home() {
  return (
    <div className={styles.container}>
      <Head>
        <title>Michael Squire Obeng</title>
        <meta name="description" content="Web Developer | Graphic Designer" />
        <link rel="icon" href="/MSOfavicon.ico" />
        <link rel="preconnect" href="https://fonts.googleapis.com" />
        <link rel="preconnect" href="https://fonts.gstatic.com" crossOrigin />
        <link href="https://fonts.googleapis.com/css2?family=Poppins:wght@300;400;500;600;700;800;900&family=Mr+Dafoe&display=swap" rel="stylesheet" /> 
      </Head>
      <div className={styles.container}>
        <Header />
          <main className={styles.main}>
            <LeftMain />
            <div className="rightMain">
              <Welcome />
              <About />
              <Portfolio />
              <Contact />
              {/* <Pricing /> */}
            </div>
          </main>
          <footer ></footer>
      </div>
    </div>
  )
}
